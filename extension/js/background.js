var debug = 1;
var start_time = Date.now();
var stop_time;
var duration;
var urls_to_compare = readTextFile("../data/stop_urls.txt");

/* this code base courtesty of https://stackoverflow.com/questions/14446447/javascript-read-local-text-file#14446538*/

function readTextFile(file){
	var rawFile = new XMLHttpRequest();
	rawFile.open("GET", file, false);
	rawFile.onreadystatechange = function (){
		if(rawFile.readyState === 4){ 
			if(rawFile.status === 200 || rawFile.status == 0){
				lines = rawFile.responseText.split("\n");
		}}}
	rawFile.send(null);
	return(lines);}

// abstracted in case we want to modify how we parse the duration
function get_duration(){
	stop_time = Date.now();
	var local_duration = stop_time - start_time;
	return local_duration;
}

function is_a_stop(possible_stop_urls, current_url){
	if (debug){
		console.log("URLs to compare");
		console.log(possible_stop_urls);
		console.log(possible_stop_urls.length);
		console.log("current URL:");
		console.log(current_url);}
	for (i = 0; i<possible_stop_urls.length; i++){
		if (current_url == possible_stop_urls[i]){
			console.log("stop url detected");
			return(true);}}
	return(false);}

chrome.tabs.onUpdated.addListener(function (tabID, changeInfo, tab) {
	if (debug){ console.log(urls_to_compare);}
	var url;

	chrome.tabs.query({'active': true, 'lastFocusedWindow': true}, function (tabs) {
		url = tabs[0].url;
		if (debug){ console.log(url);}
		if (changeInfo.status=='complete'){
			if (is_a_stop(urls_to_compare, url)){
				duration = get_duration();
				chrome.tabs.sendMessage(tab.id, { action: duration });
				if (debug) {console.log("background found match");}} } 
});});
