# About

This is Craig Earley's Earlham 2016 senior project.

More details as the code is developed.

# Code

The code will eventually include:
- db_link.php to connect to a PostgreSQL database and store a record
- manifest.json and other non-standard standard Chrome Extension baseline files
- html code for the web page

