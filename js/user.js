var startTime;
var stopTime;
var duration;
var starturl;
var stopurl;
var markers = [];

function eventInit() {
	var deferred = $.Deferred();
    console.log("Setting up");
	deferred.resolve();
	return deferred;
};

/* parse the URL, handle problems with it, do exception handling, etc. */
function checkURL(url_start){
	new_url = "https://" + url_start;
	return new_url;
}

/* wrapper for the timing function in case it needs modified */
function timestamp(){
	return Date.now();
}

$(window).load(function() {
	$("#user-button").click(function(event){
		startTime = timestamp();
		event.preventDefault();

		start_url = $("#input-text").val();
		
		//a lot to check in the URL, but for now assume it's good
		start_url = checkURL(start_url);
		window.open(start_url);		
// 		if (start_url.toLowerCase() == "advogados"){
// 		   window.location.href = "https://www.google.com";}
// 		else{console.log("entered: " + start_url);}
// 		  window.open(start_url);
		stopTime = timestamp();
		duration = stopTime-startTime;
		console.log("started " + startTime);
		console.log("stopped " + stopTime);
		console.log("duration " + duration + " ms");
	});
		
// 	$("#reset-button").click(resetform);
});


