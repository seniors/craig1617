var startTime;
var stopTime;
var duration;
var starturl;
var stopurl;
var markers = [];

function eventInit() {
	var deferred = $.Deferred();
	console.log("Setting up");
	deferred.resolve();
	return deferred;
};

/* parse the URL, handle problems with it, do exception handling, etc. */
function parse_url_list(url_start){
	url_array = url_start.split("\n")
	return url_array;
}

/* wrapper for the timing function in case it needs modified */
function timestamp(){
	return Date.now();
}

$(window).load(function() {
	$("#submit").click(function(event){
		startTime = timestamp();
		event.preventDefault();

		start_url = $("#start").val();
		stop_url = $("#stop").val();
		
		//a lot to check in the URL, but for now assume it's good
		start_url = parse_url_list(start_url);
		stop_url = parse_url_list(stop_url);
		console.log(start_url);
		console.log(stop_url);
//		window.open(start_url);		
// 		if (start_url.toLowerCase() == "advogados"){
// 		   window.location.href = "https://www.google.com";}
// 		else{console.log("entered: " + start_url);}
// 		  window.open(start_url);
		stopTime = timestamp();
		duration = stopTime-startTime;
		console.log("started " + startTime);
		console.log("stopped " + stopTime);
		console.log("duration " + duration + " ms");
		document.getElementById("output").innerHTML = stopTime;
	});
		
// 	$("#reset-button").click(resetform);
});


